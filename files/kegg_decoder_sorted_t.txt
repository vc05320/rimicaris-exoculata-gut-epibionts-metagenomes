	F-Rainbow-MAG-00001-Hepatoplasmataceae	F-Rainbow-MAG-00002-Sulfurovaceae	F-SnakePit-MAG-00001-UBA3375	F-SnakePit-MAG-00002-Hepatoplasmataceae	F-SnakePit-MAG-00003-GCA-2747515	F-SnakePit-MAG-00004-Hepatoplasmataceae	F-SnakePit-MAG-00005-Sulfurovaceae	F-SnakePit-MAG-00006-PXDI01	F-TAG-MAG-00001-Hepatoplasmataceae	F-TAG-MAG-00002-Hepatoplasmataceae	F-TAG-MAG-00003-Cardiobacteriaceae	M-Rainbow-MAG-00001-Unclassified-Deferribacteres	M-Rainbow-MAG-00002-Sulfurovaceae	M-Rainbow-MAG-00003-Mycoplasmataceae	M-SnakePit-MAG-00001-Unclassified-Lachnospirales	M-SnakePit-MAG-00002-Unclassified-Deferribacteres	M-TAG-MAG-00001-Unclassified-Lachnospirales	M-TAG-MAG-00002-Unclassified-Deferribacteres	M-TAG-MAG-00003-GCA-2747515	M-TAG-MAG-00004-Unclassified-Deferribacteres	Group
Glycolysis	 0.67	 0.22	 0.67	 0.67	 0.78	 0.67	 0.67	 0.67	 0.56	 0.67	 0.56	 0.78	 0.56	 0.44	 0.78	 0.78	 0.78	 0.78	 0.56	 0.78	Carbohydrate metabolism
Gluconeogenesis	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.78	 0.0	 0.0	 0.0	 0.67	 0.78	 0.0	 0.0	 0.0	 0.78	 0.0	 0.78	 0.67	 0.78	Carbon fixation
TCA Cycle	 0.0	 0.12	 0.0	 0.0	 0.0	 0.0	 0.38	 0.0	 0.0	 0.0	 0.38	 0.12	 0.38	 0.0	 0.12	 0.12	 0.12	 0.12	 0.0	 0.12	Carbohydrate metabolism
NAD(P)H-quinone oxidoreductase	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Oxidative phosphorylation
NADH-quinone oxidoreductase	 0.0	 0.7	 0.0	 0.0	 0.0	 0.0	 0.63	 0.07	 0.0	 0.0	 0.0	 0.35	 0.7	 0.0	 0.07	 0.35	 0.07	 0.35	 0.0	 0.35	Oxidative phosphorylation
Na-NADH-ubiquinone oxidoreductase	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.84	 0.0	 0.0	 0.84	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	Oxidative phosphorylation
F-type ATPase	 0.625	 0.75	 0.375	 0.75	 0.875	 0.375	 1.0	 0.125	 0.75	 0.625	 0.375	 0.875	 0.875	 0.25	 1.0	 0.625	 1.0	 0.875	 0.75	 0.875	Oxidative phosphorylation
V-type ATPase	0	0	0	0	0	0	0	 0.11	0	0	0	0	0	0	0	0	0	0	0	0	Oxidative phosphorylation
Cytochrome c oxidase	0	0	0	0	0	0	0	0	0	0	0	0	 0.5	0	0	0	0	0	0	0	Oxidative phosphorylation
Ubiquinol-cytochrome c reductase	0	0	0	0	0	0	 0.99	0	0	0	 0.66	0	 0.99	0	0	0	0	0	0	0	Oxidative phosphorylation
Cytochrome o ubiquinol oxidase	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Oxidative phosphorylation
Cytochrome aa3-600 menaquinol oxidase	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Oxidative phosphorylation
Cytochrome c oxidase, cbb3-type	0	0	0	0	0	0	 0.5	0	0	0	0	 0.25	 0.25	0	0	 0.25	0	 0.25	0	0	Oxidative phosphorylation
Cytochrome bd complex	0	 0.5	0	0	0	0	0	0	0	0	0	0	 1.0	0	 1.0	0	 1.0	0	0	0	Oxidative phosphorylation
RuBisCo	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Carbon fixation
CBB Cycle	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	Carbon fixation
rTCA Cycle	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Carbon fixation
Wood-Ljungdahl	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	Carbon fixation
3-Hydroxypropionate Bicycle	 0.0	 0.0	 0.0	 0.0	 0.06	 0.0	 0.06	 0.06	 0.0	 0.0	 0.0	 0.06	 0.06	 0.0	 0.12	 0.06	 0.12	 0.06	 0.06	 0.06	Carbon fixation
4-Hydroxybutyrate/3-hydroxypropionate	 0.0	 0.1	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.1	 0.0	 0.2	 0.0	 0.2	 0.0	 0.0	 0.0	Carbon fixation
Pectinesterase	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Carbon degradation
Diacetylchitobiose deacetylase	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Carbon degradation
Glucoamylase	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Carbon degradation
D-galacturonate epimerase	0	0	0	0	0	0	1	1	0	0	0	0	1	0	0	0	0	0	0	0	Carbon degradation
Exo-poly-alpha-galacturonosidase	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Carbon degradation
Oligogalacturonide lyase	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Carbon degradation
Cellulase	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Carbon degradation
Exopolygalacturonase	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Carbon degradation
Chitinase	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1	0	1	0	0	0	Carbon degradation
Basic endochitinase B	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Carbon degradation
Bifunctional chitinase/lysozyme	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Carbon degradation
Beta-N-acetylhexosaminidase	0	0	0	0	0	0	1	0	0	0	0	1	1	0	1	1	1	1	0	1	Carbon degradation
D-galacturonate isomerase	0	0	0	0	0	0	0	1	0	0	0	0	0	0	0	0	0	0	0	0	Carbon degradation
Alpha-amylase	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Carbon degradation
Beta-glucosidase	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Carbon degradation
Pullulanase	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Carbon degradation
Ammonia oxidation (amo/pmmo)	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Nitrogen metabolism
Hydroxylamine oxidation	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Nitrogen metabolism
Nitrite oxidation	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Nitrogen metabolism
Dissimilatory nitrate reduction	0	0	0	0	0	0	1	0	0	0	0	0	1	0	0	0	0	0	0	0	Nitrogen metabolism
DNRA	0	0	0	0	0	0	0	0	0	0	0	0	1	0	0	0	0	0	0	0	Nitrogen metabolism
Nitrite reduction	0	0	0	0	0	0	1	0	0	0	1	0	0	0	0	0	0	0	0	0	Nitrogen metabolism
Nitric oxide reduction	0	0	0	0	0	0	1	0	0	0	0	0	1	0	0	0	0	0	0	0	Nitrogen metabolism
Nitrous-oxide reduction	0	0	0	0	0	0	1	0	0	0	0	0	0	0	0	0	0	0	0	0	Nitrogen metabolism
Nitrogen fixation	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Nitrogen metabolism
Hydrazine dehydrogenase	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Nitrogen metabolism
Hydrazine synthase	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Nitrogen metabolism
Dissimilatory sulfate < > APS	0	0	0	0	1	0	0	0	0	0	0	0	0	0	0	0	0	0	1	0	Sulfur metabolism
Dissimilatory sulfite < > APS	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Sulfur metabolism
Dissimilatory sulfite < > sulfide	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Sulfur metabolism
Thiosulfate oxidation	 0.0	 0.64	 0.0	 0.0	 0.0	 0.0	 0.8	 0.0	 0.0	 0.0	 0.0	 0.0	 0.96	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	Sulfur metabolism
Alternative thiosulfate oxidation tsdA	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Sulfur metabolism
Alternative thiosulfate oxidation doxAD	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Sulfur metabolism
Sulfur reductase sreABC	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Sulfur metabolism
Thiosulfate/polysulfide reductase	0	0	0	0	0	0	0	0	0	0	0	0	 0.33	0	0	0	0	0	0	0	Sulfur metabolism
Sulfhydrogenase	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Sulfur metabolism
Sulfur disproportionation	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Sulfur metabolism
Sulfur dioxygenase	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Sulfur metabolism
Sulfite dehydrogenase	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Sulfur metabolism
Sulfite dehydrogenase (quinone)	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Sulfur metabolism
Sulfide oxidation	0	1	0	0	0	0	1	0	0	0	0	1	1	0	0	1	0	1	0	1	Sulfur metabolism
Sulfur assimilation	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Sulfur metabolism
DMSP demethylation	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Sulfur metabolism
DMS dehydrogenase	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Sulfur metabolism
DMSO reductase	0	0	0	0	0	0	 0.33	0	0	0	0	0	 0.33	0	0	0	0	0	0	0	Sulfur metabolism
NiFe hydrogenase	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Hydrogen redox
Ferredoxin hydrogenase	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Hydrogen redox
Membrane-bound hydrogenase	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Hydrogen redox
Hydrogen:quinone oxidoreductase	0	0	0	0	0	0	1	0	0	0	0	0	1	0	0	0	0	0	0	0	Hydrogen redox
NAD-reducing hydrogenase	0	0	0	0	0	0	0	0	0	0	0	0	 0.5	0	0	0	0	0	0	0	Hydrogen redox
NADP-reducing hydrogenase	0	0	0	0	0	0	0	0	0	0	0	0	0	0	 0.25	0	 0.25	0	0	0	Hydrogen redox
NiFe hydrogenase Hyd-1	0	0	0	0	0	0	 0.33	0	0	0	0	0	 0.33	0	0	0	0	0	0	0	Hydrogen redox
Thiamin biosynthesis	 0.09	 0.18	 0.27	 0.09	 0.18	 0.09	 0.64	 0.27	 0.09	 0.09	 0.18	 0.27	 0.73	 0.0	 0.36	 0.27	 0.36	 0.27	 0.09	 0.27	Vitamin biosynthesis
Riboflavin biosynthesis	 0.0	 0.25	 0.0	 0.0	 0.0	 0.0	 1.0	 0.25	 0.0	 0.0	 0.75	 1.0	 0.75	 0.0	 0.0	 1.0	 0.0	 1.0	 0.0	 1.0	Vitamin biosynthesis
Cobalamin biosynthesis	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.25	 0.0	 0.0	 0.5	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	Vitamin biosynthesis
Transporter: vitamin B13	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Transporters
Transporter: thiamin	0	 0.33	0	0	0	0	0	0	0	0	0	0	 0.66	0	0	0	0	0	0	0	Transporters
Transporter: urea	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	Transporters
Transporter: phosphonate	0	0	0	0	0	0	0	0	0	0	0	0	0	 0.33	0	0	0	0	0	0	Transporters
Transporter: phosphate	 0.25	0	0	 0.25	0	0	 1.0	 0.75	 0.25	 0.25	0	 1.0	 0.75	 0.25	0	 1.0	0	 1.0	0	 1.0	Transporters
Flagellum	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.83	 0.0	 0.0	 0.83	 0.83	 0.83	 0.83	 0.0	 0.83	Cell motility
Chemotaxis	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.12	 0.38	 0.0	 0.0	 0.12	 0.5	 0.12	 0.5	 0.0	 0.5	Cell motility
Methanogenesis via methanol	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Methane metabolism
Methanogenesis via acetate	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Methane metabolism
Methanogenesis via dimethylsulfide, methanethiol, methylpropanoate	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Methane metabolism
Methanogenesis via methylamine	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Methane metabolism
Methanogenesis via trimethylamine	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Methane metabolism
Methanogenesis via dimethylamine	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Methane metabolism
Methanogenesis via CO2	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Methane metabolism
Coenzyme B/Coenzyme M regeneration	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	Methane metabolism
Coenzyme M reduction to methane	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Methane metabolism
Soluble methane monooxygenase	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Methane metabolism
Methanol dehydrogenase	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Methane metabolism
Alcohol oxidase	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Methane metabolism
Dimethylamine/trimethylamine dehydrogenase	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Methane metabolism
Photosystem II	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Photosynthesis
Photosystem I	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Photosynthesis
Cytochrome b6/f complex	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Photosynthesis
Anoxygenic type-II reaction center	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Photosynthesis
Anoxygenic type-I reaction center	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Photosynthesis
Retinal biosynthesis	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Photosynthesis
Entner-Doudoroff Pathway	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.5	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.25	 0.0	 0.0	 0.0	Carbohydrate metabolism
Mixed acid: Lactate	1	0	0	1	1	1	0	0	1	1	0	0	0	0	0	0	0	0	1	0	Mixed Acid Fermentation
Mixed acid: Formate	0	0	1	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Mixed Acid Fermentation
Mixed acid: Formate to CO2 & H2	0	0	0	0	0	0	0	0	0	0	0	0	 0.167	0	0	0	0	0	0	0	Mixed Acid Fermentation
Mixed acid: Acetate	0	0	0	0	0	0	0	 0.5	0	0	 0.5	0	0	0	0	0	0	0	0	0	Mixed Acid Fermentation
Mixed acid: Ethanol, Acetate to Acetylaldehyde	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Mixed Acid Fermentation
Mixed acid: Ethanol, Acetyl-CoA to Acetylaldehyde (reversible)	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1	0	1	0	0	0	Mixed Acid Fermentation
Mixed acid: Ethanol, Acetylaldehyde to Ethanol	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Mixed Acid Fermentation
Mixed acid: PEP to Succinate via OAA, malate & fumarate	0	 0.25	0	0	0	0	 0.625	 0.25	0	0	 0.3125	 0.5205	 0.75	0	 0.125	 0.458	 0.125	 0.5205	0	 0.458	Mixed Acid Fermentation
Naphthalene degradation to salicylate	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	 0.0	Degradation of aromatic compounds
Biofilm PGA Synthesis protein	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Biofilm formation
Colanic acid and Biofilm transcriptional regulator	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Biofilm formation
Biofilm regulator BssS	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Biofilm formation
Colanic acid and Biofilm protein A	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Biofilm formation
Curli fimbriae biosynthesis	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Biofilm formation
Adhesion	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Biofilm formation
Competence-related core components	0	 0.14	 0.42000000000000004	 0.07	 0.14	0	 0.14	 0.14	 0.07	0	 0.14	 0.21000000000000002	 0.21000000000000002	0	 0.28	 0.14	 0.28	 0.21000000000000002	 0.14	 0.21000000000000002	Competence-related DNA transporter
Competence-related related components	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Competence-related DNA transporter
Competence factors	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Competence-related DNA transporter
Glyoxylate shunt	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Anaplerotic Reactions
Anaplerotic genes	0	 0.25	0	0	0	0	 0.25	 0.25	0	0	 0.5	 0.25	 0.25	0	0	 0.25	0	 0.25	0	 0.25	Anaplerotic Reactions
Sulfolipid biosynthesis	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Carbohydrate metabolism
C-P lyase cleavage PhnJ	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	C-P lyase
CP-lyase complex	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	C-P lyase
CP-lyase operon	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	C-P lyase
Type I Secretion	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Bacterial Secretion Systems
Type III Secretion	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Bacterial Secretion Systems
Type II Secretion	0	0	0	0	 0.0769	0	 0.2307	 0.2307	0	0	 0.1538	 0.3076	 0.2307	0	0	 0.3076	0	 0.3076	0	 0.3076	Bacterial Secretion Systems
Type IV Secretion	 0.083	0	0	0	0	 0.083	0	0	0	 0.083	0	0	 0.83	0	0	0	0	0	0	0	Bacterial Secretion Systems
Type VI Secretion	 0.111	0	0	0	0	 0.111	0	0	0	 0.111	0	0	0	0	0	0	0	0	0	0	Bacterial Secretion Systems
Sec-SRP	0	0	0	0	0	0	 0.5810000000000001	0	0	0	 0.5810000000000001	 0.664	 0.664	0	0	 0.664	0	 0.664	0	 0.664	Bacterial Secretion Systems
Twin Arginine Targeting	0	 0.25	0	0	0	0	 0.75	0	0	0	 0.25	0	 0.5	0	0	0	0	0	0	0	Bacterial Secretion Systems
Type Vabc Secretion	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Bacterial Secretion Systems
Serine pathway/formaldehyde assimilation	 0.2	0	 0.1	 0.2	 0.2	 0.2	 0.2	 0.2	 0.2	 0.2	 0.1	 0.30000000000000004	 0.4	 0.2	 0.1	 0.30000000000000004	 0.1	 0.30000000000000004	 0.2	 0.30000000000000004	Amino acid metabolism
Arsenic reduction	0	0	 0.25	0	0	0	 0.5	 0.75	0	0	 0.25	 0.5	 0.75	0	0	0	0	 0.5	0	 0.25	Arsenic reduction
Cobalt transporter CbiMQ	0	0	0	0	0	0	0	0	0	0	0	0	 0.5	0	0	0	0	0	0	0	Metal Transporters
Cobalt transporter CbtA	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Metal Transporters
Cobalt transporter CorA	0	0	0	0	 1.0	0	0	0	0	0	 1.0	0	0	0	0	0	0	0	 1.0	0	Metal Transporters
Nickel ABC-type substrate-binding NikA	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Metal Transporters
Copper transporter CopA	0	0	0	0	 1.0	0	0	0	0	0	0	 1.0	 1.0	0	0	0	0	0	0	0	Metal Transporters
Ferrous iron transporter FeoB	0	0	 1.0	0	0	0	 1.0	 1.0	0	0	 1.0	 1.0	 1.0	0	 1.0	 1.0	 1.0	 1.0	0	 1.0	Metal Transporters
Ferric iron ABC-type substrate-binding AfuA	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Metal Transporters
Fe-Mn transporter MntH	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Metal Transporters
Histidine	0	0	0	0	0	0	1	0	0	0	0	0	1	0	0	0	0	0	0	0	Amino acid biosynthesis
Arginine	0	0	0	0	0	0	1	0	0	0	0	0	1	0	0	0	0	0	1	0	Amino acid biosynthesis
Lysine	0	0	0	0	1	0	1	1	0	0	1	0	1	0	1	0	1	0	1	0	Amino acid biosynthesis
Serine	1	1	0	1	1	1	1	1	1	1	1	1	1	1	0	1	0	1	1	1	Amino acid biosynthesis
Threonine	0	0	0	0	1	0	0	0	0	0	0	0	1	0	1	0	1	0	1	0	Amino acid biosynthesis
Asparagine	0	0	0	0	1	0	1	1	0	0	0	0	0	0	1	0	1	0	1	0	Amino acid biosynthesis
Glutamine	0	1	0	0	0	0	1	0	0	0	0	0	1	0	1	0	1	0	0	0	Amino acid biosynthesis
Cysteine	0	0	0	0	0	0	1	1	0	0	1	0	1	0	0	0	0	0	0	0	Amino acid biosynthesis
Glycine	1	0	0	1	1	1	0	1	1	1	1	1	1	1	1	1	1	1	1	1	Amino acid biosynthesis
Proline	0	0	0	0	1	0	1	1	0	0	1	0	1	0	0	0	0	0	1	0	Amino acid biosynthesis
Alanine	0	0	0	0	0	0	0	0	0	0	1	1	0	0	1	1	1	1	0	1	Amino acid biosynthesis
Valine	0	 0.332	0	0	 0.8300000000000001	0	 0.664	 0.332	0	0	 0.498	0	 0.8300000000000001	0	0	0	0	0	 0.166	0	Amino acid biosynthesis
Methionine	0	1	0	0	0	0	1	1	0	0	0	0	1	0	0	0	0	0	0	0	Amino acid biosynthesis
Phenylalanine	0	0	0	0	1	0	0	0	0	0	1	0	0	0	0	0	0	0	1	0	Amino acid biosynthesis
Isoleucine	0	 0.332	0	0	 0.8300000000000001	0	 0.664	 0.332	0	0	 0.498	0	 0.8300000000000001	0	0	0	0	0	 0.166	0	Amino acid biosynthesis
Leucine	0	 0.25	0	0	 1.0	0	 1.0	 0.25	0	0	 0.5	0	 1.0	0	0	0	0	0	 0.25	0	Amino acid biosynthesis
Tryptophan	0	 0.5	0	0	 0.5	0	 1.0	 0.5	0	0	 0.5	0	 1.0	0	0	0	0	0	 0.5	0	Amino acid biosynthesis
Tyrosine	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Amino acid biosynthesis
Aspartate	0	0	0	0	0	0	1	1	0	0	0	0	1	0	0	0	0	0	0	0	Amino acid biosynthesis
Glutamate	0	0	0	0	0	0	1	1	0	0	0	0	1	0	0	0	0	0	0	0	Amino acid biosynthesis
PET degradation	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Polyethylene terephthalate degradation
Starch/glycogen synthesis	0	0	0	0	0	0	0	 0.33	0	0	0	 0.66	0	0	 0.99	 0.66	 0.99	 0.66	0	 0.66	Storage carbon and phosphorous
Starch/glycogen degradation	0	0	0	0	1	0	0	0	0	0	0	1	0	0	1	1	1	1	0	1	Storage carbon and phosphorous
Polyhydroxybutyrate synthesis	0	 0.167	0	0	0	0	0	0	0	0	0	0	 0.167	0	 0.167	0	 0.167	0	0	0	Storage carbon and phosphorous
Bidirectional polyphosphate	 0.5	0	0	 0.5	 0.5	 0.5	 1.0	0	 0.5	 0.5	 0.5	 0.5	 1.0	0	 0.5	 0.5	 0.5	 0.5	 0.5	 0.5	Storage carbon and phosphorous