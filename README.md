# Rimicaris Exoculata Gut Epibionts Metagenomes

This project describes the reproducible bioinformatics workflow for our study titled “A novel and dual digestive symbiosis scales up the nutrition and immune system of the holobiont *Rimicaris exoculata*”.

### Adaptor trimming

We downloaded the truseq adapters sequences for bbduk,
```
mkdir ../00_LOGS
mkdir ../01_QC
wget https://github.com/BioInfoTools/BBMap/raw/master/resources/truseq.fa.gz -P ../01_QC
```

and ran the follwing command in our work directory to remove the illumina adaptors from our sequences:

```
for r1 in *_R1.fastq.gz
do
# copy the filename, r1, to a new file name, r2 and substitute R1 in the name with R2
# this generates the name of the R2 file
    r2=$r1
    r2="${r1/_R1/_R2}"
# generate the names for the output files
    r1t=$r1
    r1t="${r1t/_R1.fastq.gz/_trimmed_R1.fastq.gz}"
    r2t=$r1
    r2t="${r2t/_R1.fastq.gz/_trimmed_R2.fastq.gz}"
    NAME=$r1
    NAME="${NAME/_R1.fastq.gz/}"
# run the bbduk command
    bbduk.sh in1=$r1 in2=$r2 out1=../01_QC/$r1t out2=../01_QC/$r2t ref=../01_QC/truseq.fa.gz stats=../01_QC/"$NAME"_stats.txt ktrim=r k=23 mink=11 hdist=1 tpe tbo >> ../00_LOGS
/bbduk.log 2>&1 
done
```

### Running the anvi'o metagenomic workflow

We used the anvi'o metagenomic workflow to perform the following step:

* Quality filter short reads using illumina-utils,
* Assemble individual samples using Megahit with metasensitive mode and minimum contig length of 1000 pb,
* Recruit reads from all samples using assembly with Bowtie2,
* Identify open reading frame and single-copy core genes,
* Annotate genes functions using COG and KEGG and add single-copy core gene taxonomy determined by the Genome Taxonomy Database (GTDB),
* Profile and merge resulting files using anvi'o,
* Cluster contigs using CONCOCT an automatic binning algorithms.

We ran the workflow the following way:

```
anvi-run-workflow -w metagenomics \
                  -c metagenomics-config.json \
                  --additional-params \
                  --cores 40
```

The config file and the samples files used in the workflow are available this way:

[samples.txt](files/samples.txt)

[config-01_megahit.json](files/metagenomics-config.json)

### Taking a look at the output files

Successful completion of the anvi’o metagenomic workflow results in various standard output directories, including `00_LOGS`, `01_QC`, `02_FASTA`, `03_CONTIGS`, `04_MAPPING`, `05_ANVIO_PROFILE`, and `06_MERGED`. Below is a brief summary of the contents of these directories:

* `00_LOGS`: Log files for every operation.

* `01_QC`: Quality-filtered short metagenomic reads and final statistics.

* `02_FASTA`: Megahit assembly of each sample in the form of FASTA files.

* `03_CONTIGS`: Anvi’o contigs databases for each assembly with HMM hits for single-copy core genes.

```
F_Rainbow-contigs.db  F_SnakePit-contigs.db  F_TAG-contigs.db  M_Rainbow-contigs.db  M_SnakePit-contigs.db  M_TAG-contigs.db
```

* `04_MAPPING`: Bowtie2 read recruitment results for assembly outputs from quality-filtered short metagenomic reads in the form of BAM files.

```
ls 04_MAPPING/
F_Rainbow  F_SnakePit  F_TAG  M_Rainbow  M_SnakePit  M_TAG
```

```
ls 04_MAPPING/F_Rainbow
F_Rainbow.bam            F_Rainbow-contigs.2.bt2  F_Rainbow-contigs.rev.1.bt2  F_SnakePit.bam.bai  M_Rainbow.bam      M_SnakePit.bam.bai
F_Rainbow.bam.bai        F_Rainbow-contigs.3.bt2  F_Rainbow-contigs.rev.2.bt2  F_TAG.bam           M_Rainbow.bam.bai  M_TAG.bam
F_Rainbow-contigs.1.bt2  F_Rainbow-contigs.4.bt2  F_SnakePit.bam               F_TAG.bam.bai       M_SnakePit.bam     M_TAG.bam.bai
```

* `05_ANVIO_PROFILE`: Anvi’o single profiles for each sample.

```
ls 05_ANVIO_PROFILE/
F_Rainbow  F_SnakePit  F_TAG  M_Rainbow  M_SnakePit  M_TAG
```

```
ls -R 05_ANVIO_PROFILE/F_Rainbow/
05_ANVIO_PROFILE/F_Rainbow/:
F_Rainbow  F_SnakePit  F_TAG  M_Rainbow  M_SnakePit  M_TAG

05_ANVIO_PROFILE/F_Rainbow/F_Rainbow:
AUXILIARY-DATA.db  import_percent_of_reads_mapped.done  PROFILE.db  RUNLOG.txt

05_ANVIO_PROFILE/F_Rainbow/F_SnakePit:
AUXILIARY-DATA.db  import_percent_of_reads_mapped.done  PROFILE.db  RUNLOG.txt

05_ANVIO_PROFILE/F_Rainbow/F_TAG:
AUXILIARY-DATA.db  import_percent_of_reads_mapped.done  PROFILE.db  RUNLOG.txt

05_ANVIO_PROFILE/F_Rainbow/M_Rainbow:
AUXILIARY-DATA.db  import_percent_of_reads_mapped.done  PROFILE.db  RUNLOG.txt

05_ANVIO_PROFILE/F_Rainbow/M_SnakePit:
AUXILIARY-DATA.db  import_percent_of_reads_mapped.done  PROFILE.db  RUNLOG.txt

05_ANVIO_PROFILE/F_Rainbow/M_TAG:
AUXILIARY-DATA.db  import_percent_of_reads_mapped.done  PROFILE.db  RUNLOG.txt
```

* `06_MERGED`: Anvi’o merged profile databases for each assembly.

```
ls -R 06_MERGED/
06_MERGED/:
F_Rainbow                    F_SnakePit                    F_TAG                    M_Rainbow                    M_SnakePit                    M_TAG
F_Rainbow-concoct.done       F_SnakePit-concoct.done       F_TAG-concoct.done       M_Rainbow-concoct.done       M_SnakePit-concoct.done       M_TAG-concoct.done

06_MERGED/F_Rainbow:
AUXILIARY-DATA.db  PROFILE.db  RUNLOG.txt

06_MERGED/F_SnakePit:
AUXILIARY-DATA.db  PROFILE.db  RUNLOG.txt

06_MERGED/F_TAG:
AUXILIARY-DATA.db  PROFILE.db  RUNLOG.txt

06_MERGED/M_Rainbow:
AUXILIARY-DATA.db  PROFILE.db  RUNLOG.txt

06_MERGED/M_SnakePit:
AUXILIARY-DATA.db  PROFILE.db  RUNLOG.txt

06_MERGED/M_TAG:
AUXILIARY-DATA.db  PROFILE.db  RUNLOG.txt
```

### Reconstructing Rimicaris exoculata gut epibiotic community genomes

We used `anvi-display-contigs-stats` to obtain contigs statistics

```
anvi-display-contigs-stats 03_CONTIGS/*-contigs.db \
                           --report-as-text \
                           --output-file 03_CONTIGS/contigs_stats.txt
```

This command gave us a TAB separated file comparing contigs statistics including the estimated genome number of each assembly:

| contigs_db             | F_Rainbow | F_SnakePit | F_TAG     | M_Rainbow | M_SnakePit | M_TAG     |
|:----------------------:|:---------:|:----------:|:---------:|:---------:|:----------:|:---------:|
| Total Length           | 158389923 | 163866312  | 210606236 | 163092280 | 145769045  | 168078924 |
| Num Contigs            | 111261    | 112999     | 147545    | 114963    | 103482     | 117474    |
| Num Contigs > 2.5 kb   | 4951      | 5441       | 6249      | 4746      | 3707       | 4785      |
| Num Contigs > 5 kb     | 626       | 834        | 683       | 555       | 408        | 605       |
| Num Contigs > 10 kb    | 101       | 179        | 100       | 131       | 105        | 134       |
| Num Contigs > 20 kb    | 18        | 41         | 35        | 35        | 62         | 66        |
| Num Contigs > 50 kb    | 3         | 6          | 10        | 10        | 21         | 18        |
| Num Contigs > 100 kb   | 0         | 2          | 2         | 0         | 4          | 6         |
| Longest Contig         | 85882     | 141948     | 152962    | 80808     | 128975     | 177320    |
| Shortest Contig        | 1000      | 1000       | 1000      | 1000      | 1000       | 1000      |
| Num Genes (prodigal)   | 155809    | 162063     | 204910    | 156996    | 139705     | 161690    |
| L50                    | 41277     | 40962      | 55038     | 42764     | 38714      | 43256     |
| L75                    | 73828     | 74362      | 97920     | 76367     | 68959      | 77777     |
| L90                    | 95755     | 96974      | 126951    | 98986     | 89202      | 101025    |
| N50                    | 1331      | 1348       | 1345      | 1325      | 1312       | 1331      |
| N75                    | 1127      | 1132       | 1134      | 1124      | 1121       | 1128      |
| N90                    | 1045      | 1047       | 1047      | 1043      | 1043       | 1045      |
| Archaea_76             | 244       | 342        | 300       | 174       | 105        | 217       |
| Bacteria_71            | 454       | 681        | 544       | 336       | 233        | 436       |
| Transfer_RNAs          | 379       | 520        | 514       | 351       | 306        | 378       |
| Ribosomal_RNAs         | 14        | 20         | 19        | 12        | 12         | 17        |
| Protista_83            | 24        | 45         | 36        | 12        | 11         | 19        |
| bacteria (Bacteria_71) | 7         | 10         | 7         | 5         | 3          | 6         |
| eukarya (Protista_83)  | 0         | 0          | 0         | 0         | 0          | 0         |
| archaea (Archaea_76)   | 0         | 0          | 0         | 0         | 0          | 0         |

We used `anvi-refine` to manually refine the six CONCOCT bins collections using the anvio interactive interface:

```
anvi-refine -c 03_CONTIGS/F_Rainbow-contigs.db \
            -p 06_MERGED/F_Rainbow/PROFILE.db \
            -C concoct -b Bin_1
```

### Identification and removal of redundant MAGs

To perform the next command in each assembly set we used the sets.txt file which lists the six assembly set

```
cat sets.txt
F_Rainbow
F_SnakePit
F_TAG
M_Rainbow
M_SnakePit
M_TAG
```

After completion of the refinement, we used the program `anvi-rename-bins` to rename metagenomic bins with >60% completion and <10% redundancy values as metagenome-assembled genomes (MAGs).

```
for SET in `cat sets.txt`
do
    anvi-rename-bins -c 03_CONTIGS/$SET-contigs.db \
                     -p 06_MERGED/$SET/PROFILE.db \
                     --collection-to-read concoct \
                     --collection-to-write final \
                     --call-MAGs \
                     --min-completion-for-MAG 60 \
                     --max-redundancy-for-MAG 10 \
                     --prefix $SET \
                     --report-file 06_MERGED/$SET-renaming_bins.txt \
                     >> 00_LOGS/$SET-anvi_rename_bins.log 2>&1
done
```

We used the program `anvi-summarize` to summarize the metagenomic binning results.

```
mkdir 08_SUMMARY_FINAL

for SET in `cat sets.txt`
do
    anvi-summarize -c 03_CONTIGS/$SET-contigs.db \
                   -p 06_MERGED/$SET/PROFILE.db \
                   -C final \
                   -o 08_SUMMARY_FINAL/$SET >> 00_LOGS/$SET-anvi_summarize_final.log 2>&1
done
```


This program generates a directory containing for each bin a FASTA file and genomic features including completion and redundancy estimates. We used the program `anvi-script-reformat-fasta` to rename scaffolds contained in each MAG according to their coassembly group, and store their FASTA files in a separate directory called `09_REDUNDANT_MAGs` for downstream analyses.

```
mkdir 09_REDUNDANT_MAGs

for SET in `cat sets.txt`
do
    anvi-estimate-genome-completeness -c 03_CONTIGS/$SET-contigs.db \
                                      -p 06_MERGED/$SET/PROFILE.db \
                                      -C final | grep MAG | awk '{print $2}'> MAGs.txt

    # go through each MAG, in each SUMMARY directory, and store a copy of the FASTA 
    # file with proper deflines in the 09_REDUNDANT_MAGs directory:
    for MAG in `cat MAGs.txt`
    do

        anvi-script-reformat-fasta 08_SUMMARY_FINAL/$SET/bin_by_bin/$MAG/$MAG-contigs.fa \
                                   --simplify-names \
                                   --prefix $MAG \
                                   -o 09_REDUNDANT_MAGs/$MAG.fa \
                                   >> 00_LOGS/$SET-anvi_script_reformat_fasta.log 2>&1
    done                     
done
```

For the identification and removal of redundant MAGs we prepared a `genomeinfo.csv` file with the MAG IDs and their redundancy and completion estimates.

```
echo -e "genome,completeness,contamination" > 09_REDUNDANT_MAGs/genomeinfo.csv
for file in 08_SUMMARY_FINAL/*/bins_summary.txt
do 
    tail -n +2 $file | awk '{print $1".fa,"$7","$8}' \
                     | grep '_MAG_00' >> 09_REDUNDANT_MAGs/genomeinfo.csv
done
```

We used dRep to dereplicate our MAGs collection. We determined that a given pair of MAGs were redundant if their ANI reached to 99% with a minimum alignment of 50%. We obtained 20 MAGs with these requirements.

```
dRep dereplicate 10_DREP -p 12 -comp 10 -con 10 -sa 0.99 \
                 --cov_thresh 0.50 -g 09_REDUNDANT_MAGs/*.fa \
                 --genomeInfo 09_REDUNDANT_MAGs/genomeinfo.csv >> 00_LOGS/drep.log
```

To better estimate the abundance of each non-redundant MAG, we profiled once again all 20 MAGs in this final collection to avoid underestimating their abundance and detection across 6 metagenomes due to the competing reference contexts that redundant MAGs provided.

```
cat 10_DREP/dereplicated_genomes/*.fa >> 10_DREP/MAGs-contigs.fa

anvi-run-workflow -w metagenomics \
                  -c metagenomics-config-MAGs.json \
                  --additional-params \
                      --cores 56 \
                      --keep-going --rerun-incomplete
```

The config file, fasta file and the samples files used in the workflow are available this way:

[metagenomics-config-MAGs.json](files/metagenomics-config-MAGs.json)

[samples_MAGs.txt](files/samples_MAGs.txt)

[fasta.txt](files/fasta.txt)

Successful completion of the anvi’o metagenomic workflow results in these five output directories: `11_MAGs/00_LOGS`, `11_MAGs/03_CONTIGS`, `11_MAGs/04_MAPPING`, `11_MAGs/05_ANVIO_PROFILE`, and `11_MAGs/06_MERGED`.

Although the anvi’o profile database in `11_MAGs/06_MERGED` describes the distribution and detection statistics of all contigs in all MAGs, it does not contain a collection that describes the contig-bin affiliations. Thanks to our previous naming consistency, here we can generate a text file that describes these connections and import this collection into the anvi'o profile database
```
for split_name in `sqlite3 11_MAGs/03_CONTIGS/MAGs-contigs.db 'select split from splits_basic_info'`
do
    # in this loop $split_name goes through names like this: F_Rainbow_MAG_00001_000000000001_split_00001,
    # F_Rainbow_MAG_00001_000000000002_split_00001, F_Rainbow_MAG_00001_00000000003_split_00001 ...; so we can extract
    # the MAG name it belongs to:
    MAG=`echo $split_name | awk 'BEGIN{FS="_"}{print $1"_"$2"_"$3"_"$4}'`
    
    # print it out with a TAB character
    echo -e "$split_name\t$MAG" >> 11_MAGs/06_MERGED/MAGs-collection.txt
done

anvi-import-collection 11_MAGs/06_MERGED/MAGs-collection.txt \
                       -c 11_MAGs/03_CONTIGS/MAGs-contigs.db \
                       -p 11_MAGs/06_MERGED/MAGs/PROFILE.db \
                       -C MAGs >> 11_MAGs/00_LOGS/MAGs-anvi_import_collection.log 2>&1
```

### Carbohydrate-active enzyme annotation

We exported the sequences of the gene calls from the contigs-db

```
anvi-get-sequences-for-gene-calls -c 11_MAGs/03_CONTIGS/MAGs-contigs.db \
                                  --export-gff3 \
                                  -o 11_MAGs/03_CONTIGS/MAGs-sequences.gff \
                                  &> 11_MAGs/00_LOGS/MAGs-get_sequences_gff.log 2>&1

sed -i 's/ID=/ID=genecall_/' 11_MAGs/03_CONTIGS/MAGs-sequences.gff
```

We annnotated the gene calls with dbCAN2 using the following command:

```
run_dbcan.py 11_MAGs/03_CONTIGS/MAGs-protein-sequences.fa protein \
             -c 11_MAGs/03_CONTIGS/MAGs-sequences.gff \
             --hmm_cpu 8 --dia_cpu 8 --tf_cpu 8 --hotpep_cpu 8 --stp_cpu 8 \
             --out_dir 11_MAGs/14_dbCAN2 \
             --db_dir /home/ref-bioinfo/tools_data/dbcan2 \
             &> 00_LOGS/MAGs-dbcan.log 2>&1
```

dbCAN2 combine three database/annotations methods for CAZymes. We parsed the ouput of dbCAN2 and retained the CAZymes annotations if they were found by at least two methods, as suggested by [Zang et al, (2018)](https://doi.org/10.1093/nar/gky418)

```
echo -e "gene_callers_id\tsource\taccession\tfunction\te_value" >> 11_MAGs/14_dbCAN2/overview_parsed.txt
grep "^genecall" 11_MAGs/14_dbCAN2/overview.txt | while read line
do  
    NbTool=`echo $line | awk '{print $5}'`
    DIAMOND=`echo $line | awk '{print $4}'` 
    HMMER=`echo $line | awk '{print $2}' | sed -e 's/([^)]*)//g'`
    GENE=`echo $line | awk '{print $1}' | sed -e 's/genecall_//g'`
        if [ "$NbTool" -ge 2 ] && [ "$DIAMOND" != '-' ]; 
        then echo -e $GENE"\tdbCAN2\t\t"$DIAMOND"\t0" >> 11_MAGs/14_dbCAN2/overview_parsed.txt
        elif [ "$NbTool" -ge 2 ] && [ "$HMMER" != '-' ]
        then echo -e $GENE"\tdbCAN2\t\t"$HMMER"\t0" >> 11_MAGs/14_dbCAN2/overview_parsed.txt 
        fi
done

```

We then imported the function into the contig.db

```
anvi-import-functions -c 11_MAGs/03_CONTIGS/MAGs-contigs.db \
                      -i 11_MAGs/14_dbCAN2/overview_parsed.txt \
                      >> 11_MAGs/00_LOGS/MAGs-anvi_import_functions_dbcan2.log 2>&1

```

Once we imported our CAZyme annotation, we summarized our MAGs collections

```
anvi-summarize -c 11_MAGs/03_CONTIGS/MAGs-contigs.db \
               -p 11_MAGs/06_MERGED/MAGs/PROFILE.db \
               -C MAGs \
               -o 11_MAGs/08_SUMMARY >> 11_MAGs/00_LOGS/MAGs-anvi_summarize.log 2>&1
```

From the `08_SUMMARY` folder we exported the CAZYmes annotation for each MAG

```
tail -n +2 11_MAGs/08_SUMMARY/bin_by_bin/*/*-gene_calls.txt | while read line
do
    GENE=`echo "$line" | awk 'BEGIN{FS="\t"}{print "genecall_"$1}'`
    CAZYME=`echo "$line" | awk 'BEGIN{FS="\t"}{print $18}'`
    MAG=`echo "$line" | awk -F"\t|_" '{print $2"_"$3"_"$4"_"$5}'`
if
    [ -z "$CAZYME" ]
then
    continue
else
    echo -e $GENE"\t"$MAG"\t"$CAZYME >> 11_MAGs/14_dbCAN2/dbCAN2_table.txt
fi 
done
``` 

Using R we produced a barplot showing the CAZymes family in relation to the MAG family

```
library(dplyr)
library(tidyr)
library(ggplot2)
library(stringr)
library(tidyverse)
library(RColorBrewer)

# Import dbCAN2 table
data <- read.table("dbCAN2_table.txt", sep="\t")
dbCAN2 <- data %>% 
  dplyr::rename(genecall = V1, MAG = V2, CAZy = V3) %>%
  mutate(CAZy_class=case_when(grepl("CE", CAZy) ~ "CE", grepl("CBM", CAZy) ~ "CBM", grepl("GH", CAZy) ~ "GH", grepl("GT", CAZy) ~ "GT", grepl("PL", CAZy) ~ "PL"))

# Import MAGs taxonomy
Tax <- read.csv("gtdbtk.bac120.summary_gtdbtk_v1.5.0.tsv", sep="\t", dec = ".")
Classification <- Tax %>%
  select(user_genome, classification) %>%
  separate(classification, c("Domain", "Phylum", "Class", "Order", "Family", "Genus", "Specie"), sep = ";") %>%
  dplyr::rename(MAG = user_genome) %>%
  mutate(Specie = str_replace(Specie, "s__", "")) %>%
  mutate(Genus = str_replace(Genus, "g__", "")) %>%
  mutate(Family = str_replace(Family, "f__", "")) %>%
  mutate(Order = str_replace(Order, "o__", "")) %>%
  mutate(Class = str_replace(Class, "c__", "")) %>%
  mutate(Phylum = str_replace(Phylum, "p__", "")) %>%
  mutate(Domain = str_replace(Domain, "d__", "")) %>%
  mutate(Family = ifelse(Order %in% "", paste("Unclassified_", Class, sep=""), Family)) %>%
  mutate(Family = ifelse(Family %in% "", paste("Unclassified_", Order, sep=""), Family)) %>%
  mutate(Order = ifelse(Order %in% "", paste("Unclassified_", Class, sep=""), Order))

## Plot CAZYmes family at the taxa family level

Family <- Classification %>% 
  inner_join(dbCAN2, by="MAG") %>%
  select(-MAG, -genecall) %>%
  count(CAZy, Family, name = "gene_number")
  
f <- ggplot(data=Family, aes(x= gene_number, y = CAZy, fill=Family)) +
  geom_bar(stat = "identity") +
  scale_fill_manual(values = c("Cardiobacteriaceae" = "#8F49B5", "GCA-2747515" = "#94CE53", "Mycoplasmataceae" = "#7AB592", "PXDI01" = "#9E9BC4", "Sulfurovaceae" = "#4C3A41", "UBA3375" = "#BA8F48", "Unclassified_Deferribacteres" = "#C4C4C4", "Unclassified_Lachnospirales" = "grey40")) + 
  theme_light() +
  ylab("CAZYmes family") + xlab("Number of gene in MAG") + labs(fill = "MAG family")
f

``` 

![CAZymes](files/FigureS3.png)

### Taxonomical identification of MAGs

Taxonomic assignments of our MAGs was made with GTDB-Tk based on the Genome Database Taxonomy GTDB,  

```
mkdir -p 11_MAGs/09_PHYLOGENO_GTDB/MAGs

for file in 11_MAGs/08_SUMMARY/bin_by_bin/*/*-contigs.fa
do
    file2=$(basename $file | sed 's/-contigs.fa/.fna/g')
    cp $file 11_MAGs/09_PHYLOGENO_GTDB/MAGs/$file2
done

gtdbtk classify_wf --cpus 33 --genome_dir 11_MAGs/09_PHYLOGENO_GTDB/MAGs \
                   --out_dir 11_MAGs/09_PHYLOGENO_GTDB/classify_wf_output_gtdbtk_v1.5.0 \
                   >> 11_MAGs/00_LOGS/MAGs-gtdbtk_v1.5.0_classify_wf.log 2>&1
```

### Phylogenetic tree reconstruction for Deferribacteres and Hepatoplasmataceae MAGs

For each clade, closest GTDB references genomes were selected to perform an alignment of bacterial marker genes with our MAGs

```
mkdir -p 11_MAGs/09_PHYLOGENO_GTDB/MAGs_hepatoplasmataceae/

for MAG in F_Rainbow_MAG_00001.fna F_SnakePit_MAG_00004.fna F_TAG_MAG_00002.fna F_SnakePit_MAG_00002.fna F_TAG_MAG_00001.fna
do
    cp 11_MAGs/09_PHYLOGENO_GTDB/MAGs/$MAG 11_MAGs/09_PHYLOGENO_GTDB/MAGs_hepatoplasmataceae/$MAG
done

gtdbtk classify_wf --cpus 33 --genome_dir 11_MAGs/09_PHYLOGENO_GTDB/MAGs_hepatoplasmataceae \
                   --out_dir 11_MAGs/09_PHYLOGENO_GTDB/classify_wf_output_hepatoplasmataceae \
                   >> 11_MAGs/00_LOGS/MAGs-gtdbtk_v1.5.0_classify_wf_hepatoplasmataceae.log 2>&1


mkdir -p 11_MAGs/09_PHYLOGENO_GTDB/MAGs_deferribacteres

for MAG in M_Rainbow_MAG_00001.fna M_SnakePit_MAG_00002.fna M_TAG_MAG_00002.fna M_TAG_MAG_00004.fna
do
    cp 11_MAGs/09_PHYLOGENO_GTDB/MAGs/$MAG 11_MAGs/09_PHYLOGENO_GTDB/MAGs_deferribacteres/$MAG
done

gtdbtk classify_wf --cpus 33 --genome_dir 11_MAGs/09_PHYLOGENO_GTDB/MAGs_deferribacteres \
                   --out_dir 11_MAGs/09_PHYLOGENO_GTDB/classify_wf_output_deferribacteres \
                   >> 11_MAGs/00_LOGS/MAGs-gtdbtk_v1.5.0_classify_wf_deferribacteres.log 2>&1

```

```
gtdbtk align --identify_dir 11_MAGs/09_PHYLOGENO_GTDB/classify_wf_output_hepatoplasmataceae \
             --out_dir 11_MAGs/09_PHYLOGENO_GTDB/align_output_hepatoplasmataceae \
             --cpus 33 \
             --taxa_filter o__Mycoplasmatales \
             >> 11_MAGs/00_LOGS/MAGs-gtdbtk_v1.5.0_align_hepatoplasmataceae.log 2>&1

gtdbtk align --identify_dir 11_MAGs/09_PHYLOGENO_GTDB/classify_wf_output_deferribacteres \
             --out_dir 11_MAGs/09_PHYLOGENO_GTDB/align_output_deferribacteres_rooted \
             --cpus 33 \
             --taxa_filter c__Deferribacteres \
             >> 11_MAGs/00_LOGS/MAGs-gtdbtk_v1.5.0_align_deferribacteres.log 2>&1
```

Alignments were cleaned up by removing positions that were gap characters in more than 50% of the sequences using trimAL

```
mkdir -p 11_MAGs/09_PHYLOGENO_GTDB/tree_hepatoplasmataceae

trimal -in 11_MAGs/09_PHYLOGENO_GTDB/align_output_hepatoplasmataceae/gtdbtk.bac120.msa.fasta \
       -out 11_MAGs/09_PHYLOGENO_GTDB/tree_hepatoplasmataceae/gtdbtk_bac120_msa_anvio_clean.fasta \
       -gt 0.50 >> 11_MAGs/00_LOGS/MAGs-trimal-hepatoplasmataceae.log 2>&1

mkdir -p 11_MAGs/09_PHYLOGENO_GTDB/tree_deferribacteres

trimal -in 11_MAGs/09_PHYLOGENO_GTDB/align_output_deferribacteres/gtdbtk.bac120.msa.fasta \
       -out 11_MAGs/09_PHYLOGENO_GTDB/tree_deferribacteres/gtdbtk_bac120_msa_anvio_clean.fasta \
       -gt 0.50 >> 11_MAGs/00_LOGS/MAGs-trimal-deferribacteres.log 2>&1
```

And we built maximum likelihood trees using IQ-TREE with the 'WAG' general matrice model.

```
iqtree -s 11_MAGs/09_PHYLOGENO_GTDB/tree_hepatoplasmataceae/gtdbtk_bac120_msa_anvio_clean.fasta \
       -nt 33 \
       -m WAG \
       -bb 1000 >> 11_MAGs/00_LOGS/MAGs-iqtree-hepatoplasmataceae.log 2>&1

iqtree -s 11_MAGs/09_PHYLOGENO_GTDB/tree_deferribacteres/gtdbtk_bac120_msa_anvio_clean.fasta \
       -nt 33 \
       -m WAG \
       -bb 1000 >> 11_MAGs/00_LOGS/MAGs-iqtree-deferribacteres.log 2>&1
```

Now we have a newick tree that shows our MAG with the closest GTDB genomes. We used FigTree to visualize and annotate these new trees:

![Phylogenomic Trees](files/Figure3.png)

### Compute average nucleotide identity (ANI) for Deferribacteres and Hepatoplasmataceae MAGs and references GTDB genomes

For each clade we calculated average nucleotide identity (ANI) using `anvi-compute-genome-similarity` from anvi’o with the pyANI program

```
anvi-compute-genome-similarity -f 11_MAGs/13_ANI/fasta_deferribacteres_ref.txt \
                               -o 11_MAGs/13_ANI/output_Deferribacteres_ref \
                               --program pyANI --min-alignment-fraction 0 \
                               --log-file 11_MAGs/00_LOGS/MAGs-anvi_compute_genome_similarity_deferribacteres_ref.log

anvi-compute-genome-similarity -f 11_MAGs/13_ANI/fasta_hepatoplasmataceae_ref.txt \
                               -o 11_MAGs/13_ANI/output_Hepatoplasmataceae_ref \
                               --program pyANI --min-alignment-fraction 0 \
                               --log-file 11_MAGs/00_LOGS/MAGs-anvi_compute_genome_similarity_Hepatoplasmataceae_ref.log
```
The fasta.txt files and the ANI result files are available this way:

[fasta_deferribacteres_ref.txt](files/fasta_deferribacteres_ref.txt)

[fasta_hepatoplasmataceae_ref.txt](files/fasta_hepatoplasmataceae_ref.txt)

[Deferribacteres_ANIb_percentage_identity.txt](files/Deferribacteres_ANIb_percentage_identity.txt)

[Hepatoplasmataceae_ANIb_percentage_identity.txt](files/Hepatoplasmataceae_ANIb_percentage_identity.txt)

### Generate anvi'o static image

For the anvi'o static image of the MAG we built a phylogenomic tree including only our MAG:

```
mkdir -p 11_MAGs/09_PHYLOGENO_GTDB/anvio_static_image

# Make tree with the MSA file from gtdb_tk
trimal -in 11_MAGs/09_PHYLOGENO_GTDB/classify_wf_output_gtdbtk_v1.5.0/gtdbtk.bac120.user_msa.fasta \
       -out 11_MAGs/09_PHYLOGENO_GTDB/anvio_static_image/gtdbtk_bac120.user_msa_anvio_clean.fasta \
       -gt 0.50 &> 11_MAGs/00_LOGS/MAGs-trimal_static_image.log 2>&1 

iqtree -s 11_MAGs/09_PHYLOGENO_GTDB/anvio_static_image/gtdbtk_bac120.user_msa_anvio_clean.fasta \
       -nt 8 \
       -m WAG \
       -bb 1000 &> 11_MAGs/00_LOGS/MAGs-iqtree_static_image.log 2>&1
```
Now we have a newick tree that shows our MAG. We used the anvi'o interactive interface to visualize this new tree:

```
anvi-interactive -t 11_MAGs/09_PHYLOGENO_GTDB/anvio_static_image/gtdbtk_bac120.user_msa_anvio_clean.fasta.contree \
                 -p 11_MAGs/12_TABLES/profile_gtdb_v1_5_0.db \
                 --title "anvi'o static image" \
                 --manual
```

![Static image 1](files/anvi_o_static_image_v1.png)

We used the program `anvi-estimate-genome-completeness` to export the estimate completion, redundancy and the genome length obtained using anvio

```
anvi-estimate-genome-completeness -c 11_MAGs/03_CONTIGS/MAGs-contigs.db \
                                  -p 11_MAGs/06_MERGED/MAGs/PROFILE.db \
                                  -C MAGs \
                                  -o 11_MAGs/08_SUMMARY/MAGs-genome-completeness.txt \
                                  >> 11_MAGs/00_LOGS/MAGs-anvi_estimate_genome_completeness.log 2>&1

echo -e "Completion\tRedundancy\tLength (Mbp)\tMAG name" > 11_MAGs/12_TABLES/Completion.txt
tail -n +2 11_MAGs/08_SUMMARY/MAGs-genome-completeness.txt \
    | awk -F"\t" '{print $4"\t"$5"\t"$7"\t"$1}' >> 11_MAGs/12_TABLES/Completion.txt
```
We retrived the class and families annotation of the MAG from GTDB_Tk classify workflow and produceed a table with the GC content, length, completion, rendundancy and mean coverage obtained with anvio
```
echo -e "Class" > 11_MAGs/12_TABLES/Class_gtdb_v1_5_0.txt
tail -n +2 11_MAGs/09_PHYLOGENO_GTDB/classify_wf_output_gtdbtk_v1.5.0/gtdbtk.bac120.summary.tsv \
    | sort | awk -F";|\t" '{print $4}' | sed -e "s/.__//g" >> 11_MAGs/12_TABLES/Class_gtdb_v1_5_0.txt

echo -e "Families" > 11_MAGs/12_TABLES/Families_gtdb_v1_5_0.txt
tail -n +2 11_MAGs/09_PHYLOGENO_GTDB/classify_wf_output_gtdbtk_v1.5.0/gtdbtk.bac120.summary.tsv \
    | sort | awk -F";|\t" '{print $6}' | sed -e "s/.__//g" | sed -e "s/^$/NA/g" >> 11_MAGs/12_TABLES/Families_gtdb_v1_5_0.txt

echo -e "GC content" > 11_MAGs/12_TABLES/GC.txt
for GC in `cat 11_MAGs/08_SUMMARY/bin_by_bin/*/*-GC_content.txt`
do
    echo -e $GC >> 11_MAGs/12_TABLES/GC.txt
done

paste 11_MAGs/08_SUMMARY/bins_across_samples/mean_coverage.txt 11_MAGs/12_TABLES/GC.txt \
      11_MAGs/12_TABLES/Completion.txt 11_MAGs/12_TABLES/Families_gtdb_v1_5_0.txt \
      11_MAGs/12_TABLES/Class_gtdb_v1_5_0.txt > 11_MAGs/12_TABLES/items-data_gtdb_v1_5_0.txt
```
With R we also produced a data matrix representing the percent of reads recruited to the bins for each sample at the family level. To generate this matrix we used an output from the `anvi-summarize` and `gtdbtk classify_wf`:
```
Tax <- read.csv("11_MAGs/09_PHYLOGENO_GTDB/classify_wf_output_gtdbtk_v1.5.0/gtdbtk.bac120.summary.tsv", sep="\t", dec = ".")
Pct_recr <- read.csv("11_MAGs/08_SUMMARY/bins_across_samples/bins_percent_recruitment.txt",
                     sep="\t", dec = ".", header = FALSE, row.names = 1)

library(dplyr)
library(tidyverse)

Pct_recr_t <- as.data.frame(t(Pct_recr))
Pct_recr_t <- Pct_recr_t %>% 
  filter(samples != "__splits_not_binned__") %>%
  rename(MAGs = samples) 

Classification <- Tax %>%
  select(user_genome, classification) %>% 
  separate(classification, c("domain", "kingdom", "class", "order", "family", "genus", "species"), 
           sep = ";") %>%
  rename(MAGs = user_genome) %>%
  select(MAGs, family) %>%
  right_join(Pct_recr_t, by="MAGs") %>%
  mutate_at(vars(F_Rainbow:M_TAG), as.character) %>%
  mutate_at(vars(F_Rainbow:M_TAG), as.numeric) %>%
  group_by(family) %>%
  summarise_at(vars(F_Rainbow:M_TAG), funs(sum)) %>%
  mutate(family = str_replace(family, "f__", "families!"))

Classification_sum <- Classification %>%
  mutate(Sum = F_Rainbow + F_SnakePit + F_TAG + M_Rainbow + M_SnakePit + M_TAG)

Other <- Classification_sum[Classification_sum$Sum <= 3,]$family

Classification_sum[Classification_sum$family %in% Other,]$family <- 'families!Other'

Classification_sum2 <- Classification_sum %>%
  group_by(family) %>%
  summarise_at(vars(F_Rainbow:M_TAG), funs(sum)) %>%
  rename(samples = family)
 
Classification_sum2$samples[Classification_sum2$samples == "families!"] <- "families!NA"

Classification_t <- as.data.frame(t(Classification_sum2))

write.table(Classification_t, file = "11_MAGs/12_TABLES/family_gtdb_v1_5_0.txt", 
            sep = "\t", col.names = FALSE, quote = FALSE)
```
We completed the default table from the `anvi-summarize` command output with this family table 

```
cut -f 2- 11_MAGs/08_SUMMARY/misc_data_layers/default.txt | paste 11_MAGs/12_TABLES/family_gtdb_v1_5_0.txt - \
          >> 11_MAGs/12_TABLES/layers-data_gtdb_v1_5_0.txt
```
We used the program `anvi-import-misc-data` to import these informations back into the anvi’o profile databases.

```
anvi-import-misc-data 11_MAGs/12_TABLES/items-data_gtdb_v1_5_0.txt \
                      -p 11_MAGs/12_TABLES/profile_gtdb_v1_5_0.db \
                      --target-data-table items

anvi-import-misc-data -p 11_MAGs/12_TABLES/profile_gtdb_v1_5_0.db \
                      11_MAGs/12_TABLES/layers-data_gtdb_v1_5_0.txt \
                      --target-data-table layers

```
We visualized our tree and tables in the interactive interface of anvio

```
anvi-interactive -t 11_MAGs/09_PHYLOGENO_GTDB/anvio_static_image/gtdbtk_bac120.user_msa_anvio_clean.fasta.contree \
                 --title "anvi'o static image" \
                 -p 11_MAGs/12_TABLES/profile_gtdb_v1_5_0.db \
                 --manual
```

We then manually organized the order of items with the MAGs phylogenomic tree. We changed the colors and size of the layers. Then Inkscape was used to achieve this display:

![Static image 2](files/Figure1.png)

### MAGs pathways visualisation 

To visualise the metabolic pathways and their completion level we used KEGG_DECODER

We exported the taxonomical information of our MAGs obtained with GTDB_Tk to add it in the name of the MAG using this command:

```
mkdir -p 11_MAGs/10_KEGG_DECODER/gtdbtk_v1.5.0

tail -n +2 11_MAGs/09_PHYLOGENO_GTDB/classify_wf_output_gtdbtk_v1.5.0/gtdbtk.bac120.summary.tsv | while read line
do
    FAMILY=`echo $line | awk -F" |;" '{print $6}' | sed -e 's/f__//g'`
    ORDER=`echo $line | awk -F" |;" '{print $5}' | sed -e 's/o__//g'`
    CLASS=`echo $line | awk -F" |;" '{print $4}' | sed -e 's/c__//g'`
    MAG=`echo $line | awk -F" |;" '{print $1}'`
if
    [ -z "$ORDER" ]
then
    echo -e $MAG"-Unclassified-"$CLASS >> 11_MAGs/10_KEGG_DECODER/gtdbtk_v1.5.0/MAGs-taxonomy.txt
elif
    [ -z "$FAMILY" ]
then
    echo -e $MAG"-Unclassified-"$ORDER >> 11_MAGs/10_KEGG_DECODER/gtdbtk_v1.5.0/MAGs-taxonomy.txt
else
    echo -e $MAG"-"$FAMILY >> 11_MAGs/10_KEGG_DECODER/gtdbtk_v1.5.0/MAGs-taxonomy.txt
fi
done

```
The following output provides a glimpse from the file `MAGs-taxonomy.txt`:

```
head 11_MAGs/10_KEGG_DECODER/gtdbtk_v1.5.0/MAGs-taxonomy.txt 
F_Rainbow_MAG_00001-Hepatoplasmataceae
F_Rainbow_MAG_00002-Sulfurovaceae
F_SnakePit_MAG_00001-UBA3375
F_SnakePit_MAG_00002-Hepatoplasmataceae
F_SnakePit_MAG_00003-GCA-2747515
F_SnakePit_MAG_00004-Hepatoplasmataceae
F_SnakePit_MAG_00005-Sulfurovaceae
F_SnakePit_MAG_00006-PXDI01
F_TAG_MAG_00001-Hepatoplasmataceae
F_TAG_MAG_00002-Hepatoplasmataceae

```

We retrieved the KEGG Orthology of the genes from the `anvi-summarize` program to built the matrice input for KEGG DECODER

```
for line in `cat  11_MAGs/10_KEGG_DECODER/gtdbtk_v1.5.0/MAGs-taxonomy.txt`
do
    MAGs=$(echo $line | awk 'BEGIN{FS="-"}{print $1}')
    TAX=`echo $line | sed 's/_/-/g'`
    tail -n +2 11_MAGs/08_SUMMARY/bin_by_bin/$MAGs/$MAGs-gene_calls.txt \
         | awk 'BEGIN{FS="\t"}{print "'$TAX'""_"$1"\t"$27}' \
         >> 11_MAGs/10_KEGG_DECODER/gtdbtk_v1.5.0/keggdecoder_input.txt
done

```
The following output provides a glimpse from the file `keggdecoder_input.txt`:

```
head 11_MAGs/10_KEGG_DECODER/gtdbtk_v1.5.0/keggdecoder_input.txt 
F-Rainbow-MAG-00001-Hepatoplasmataceae_0	K03438
F-Rainbow-MAG-00001-Hepatoplasmataceae_1	K03925
F-Rainbow-MAG-00001-Hepatoplasmataceae_2	
F-Rainbow-MAG-00001-Hepatoplasmataceae_3	K02911
F-Rainbow-MAG-00001-Hepatoplasmataceae_4	
F-Rainbow-MAG-00001-Hepatoplasmataceae_5	K01890
F-Rainbow-MAG-00001-Hepatoplasmataceae_6	K01889
F-Rainbow-MAG-00001-Hepatoplasmataceae_7	
F-Rainbow-MAG-00001-Hepatoplasmataceae_8	
F-Rainbow-MAG-00001-Hepatoplasmataceae_9

```
We ran the following command to generate the matrice heatmap:

```
KEGG-decoder -i 11_MAGs/10_KEGG_DECODER/gtdbtk_v1.5.0/keggdecoder_input.txt \
             -o 11_MAGs/10_KEGG_DECODER/gtdbtk_v1.5.0/keggdecoder_out.txt \
             -v static >> 11_MAGs/00_LOGS/MAGs-keggdecoder-gtdbtk-v1.5.0.log 2>&1

```
We sorted the KEGG_DECODER output and transposed it with datamash,

```
sort 11_MAGs/10_KEGG_DECODER/gtdbtk_v1.5.0/keggdecoder_out.txt | datamash transpose \
> 11_MAGs/10_KEGG_DECODER/gtdbtk_v1.5.0/kegg_decoder_sorted_t.txt

```
We also manually defined group in order to gather the metabolic pathway. The modified `kegg_decoder_sorted_t.txt` file is available this way:

[kegg_decoder_sorted_t.txt](files/kegg_decoder_sorted_t.txt)

And we produced the heatmap using R, we removed the null completion pathways. We ordered the MAGs on the heatmap based on their euclidean distances and indicated the MAGs completion, class and phylum.
```
library(ComplexHeatmap)
library(circlize)
library(RColorBrewer)
library(dplyr)
library(tidyr)

# Import KEGG pathways table as well as taxonomy obtained with GTDB_TK 
# and the completeness obtained from anvio
KEGG_t <- read.csv("11_MAGs/10_KEGG_DECODER/gtdbtk_v1.5.0/kegg_decoder_sorted_t.txt", sep="\t", dec = ".", row.names = 1)
Tax <- read.csv("11_MAGs/09_PHYLOGENO_GTDB/classify_wf_output_gtdbtk_v1.5.0/gtdbtk.bac120.summary.tsv", sep = "\t", dec = ".", row.names = 1 )
Completeness <- read.csv("11_MAGs/08_SUMMARY/MAGs-genome-completeness.txt", sep = "\t", dec =".", row.names = 1)

# Extract and sort taxonomy
Tax$classification <- gsub(".__", "", Tax$classification)
Tax <- Tax %>%
  select(classification) %>%
  separate(classification, c("kingdom","phylum","class","order","family","genus","specie"), sep = ";")
Tax_sort <- Tax[ order(row.names(Tax)), ]

# Select only completeness column
Completeness <- Completeness %>%
  select(X..completion) %>%
  rename(Completion = X..completion)

# Merge taxonomy and completeness into a single table
Tax_sort <-  merge(Tax_sort, Completeness, by=0)
rownames(Tax_sort) <- Tax_sort$Row.names
Tax_sort$Row.names <- NULL

# Remove low completion pathways
KEGG_group <- KEGG_t %>%
  select(Group)
KEGG_num_filter <- KEGG_t[, grep("MAG.00", colnames(KEGG_t))] %>%
  filter(apply(., 1, function(x) any(x > 0)))
KEGG_num_filter_group <-  merge(KEGG_num_filter, KEGG_group, by=0)
rownames(KEGG_num_filter_group) <- KEGG_num_filter_group$Row.names
KEGG_num_filter_group$Row.names <- NULL

KEGG_subset <- KEGG_num_filter_group

KEGG_s = as.matrix(KEGG_subset[, grep("MAG.00", colnames(KEGG_subset))])

hm.palette <- colorRampPalette(rev(brewer.pal(9, 'YlOrRd')), space='Lab')


c = Tax_sort$class
p = Tax_sort$phylum
comp = Tax_sort$Completion

group_order_s <- factor(KEGG_subset$Group, levels=c("Carbon fixation", "Carbohydrate metabolism",
                                                    "Carbon degradation", "Mixed Acid Fermentation", 
                                                    "Storage carbon and phosphorous", "Nitrogen metabolism", 
                                                    "Sulfur metabolism",  "Oxidative phosphorylation", 
                                                    "Hydrogen redox", "Anaplerotic Reactions", "Amino acid metabolism",
                                                    "Amino acid biosynthesis", "Vitamin biosynthesis", 
                                                    "Cell motility", "Bacterial Secretion Systems",
                                                    "Transporters", "Competence-related DNA transporter", 
                                                    "Metal Transporters", "Arsenic reduction"))

colourCount_c = length(unique(c))
getPalette_c = colorRampPalette(brewer.pal(8, "Dark2"))
c_col = structure(getPalette_c(colourCount_c), names = unique(c))

col_comp = colorRamp2(c(60, 80, 100), c("grey80", "grey40", "grey0"))

ht_opt(legend_title_gp = gpar(fontsize = 8, fontface = "bold"), 
       legend_labels_gp = gpar(fontsize = 8))

taxo = HeatmapAnnotation(Phylum = p, Class = c, Completion = comp,
                         annotation_name_side = "left", na_col = "white",
                         annotation_name_gp = gpar(fontsize = c(8)),
                         annotation_legend_param = list(Completion = list(title = "MAG completion")),
                         col = list(Phylum = c("Firmicutes" = "#638bcb", "Campylobacterota" = "#4aac8d",
                                               "Firmicutes_A" = "#7964ce", "Deferribacterota" = "#64ac48",
                                               "Proteobacteria" = "#cf5568", "Verrucomicrobiota" = "#d38cca",
                                               "Patescibacteria" = "#c950b5"),
                                    Class = c("Bacilli" = "#c9557e", "Campylobacteria" = "#6587cd", "Clostridia" = "#cc6540", 
                                              "Deferribacteres"="#a7943f", "Gammaproteobacteria" = "#a863c3", 
                                              "Kiritimatiellae" = "#4aac8d", "Paceibacteria" = "#64ac48"),                                    
                                    Completion = col_comp))

annotation_titles = c(Completion = "MAG completion")

ht_kegg_s = Heatmap(KEGG_s, name="Pathways completeness", cluster_rows = FALSE, 
                    rect_gp = gpar(col = "black", lwd = 0.5), 
                    col=colorRamp2(c(0, 0.125, 0.250, 0.375, 0.5, 0.625, 0.75, 0.875, 1), 
                                   brewer.pal(n=9, name="YlOrRd")),
                    top_annotation = taxo,
                    row_split = group_order_s, row_title_rot = 0, 
                    row_title_gp = gpar(col = c("black"), fontsize = c(8)), 
                    row_names_gp = gpar(col = c("black"), fontsize = c(8)),
                    clustering_distance_columns = "euclidean",
                    column_names_gp = gpar(col = c("black"), fontsize = c(8)))

draw(ht_kegg_s, merge_legends = TRUE, annotation_legend_side = "right")
```

![KEGG_DECODER Heatmap](files/FigureS2.png)

### Differentially Abundant MAGs between digestive tract (Fore- and midgut)

To identify differentially abundant MAGs between digestive tract we retrieved the raw counts mapping for each MAG from the .bam file using `samtools view`.

```
mkdir 11_MAGs/11_DESeq2

for file in 11_MAGs/08_SUMMARY/bin_by_bin/*/*-contigs.fa
do
    cat $file | grep '^>' | sed 's/>//' | sed ':a;N;$!ba;s/\n/ /g' >> 11_MAGs/11_DESeq2/list_contigs_MAGs.txt
done

# Run samtools view for each contigs of MAGs
cat 11_MAGs/11_DESeq2/list_contigs_MAGs.txt | while read CONTIGS
do
    for BAM in 11_MAGs/04_MAPPING/MAGs/*.bam
    do
        SAMPLE="$(basename $BAM | sed 's/.bam//g')"
        MAG=`echo $CONTIGS | cut -f1 -d" " | cut -f -4 -d"_"`
        COUNT=`samtools view -c $BAM $CONTIGS`
        echo -e $SAMPLE"\t"$MAG"\t"$COUNT >> 11_MAGs/11_DESeq2/samtools.txt
    done
done
```
With R we imported the number of aligned reads obtained from samtools view.

```
## load library
library(dplyr)
library(tidyr)
library(DESeq2)
library(ggplot2)
library(stringr)
library(tidyverse)
library(edgeR)

# import raw reads dataset
data <- read.table("11_MAGs/11_DESeq2/samtools.txt", sep="\t")
count <- data %>% 
  dplyr::rename(Sample = V1, MAG = V2, Raw_reads = V3) %>% 
  spread(Sample, Raw_reads)
```

We imported the MAGs length from the `MAGs-genome-completeness.txt` file and we normalized mapping data within and between samples with the Gene length corrected Trimmed Mean of M-values (GeTMM) method using genome length instead of gene length.

```
# Import MAGs-genome-completeness.txt file
completeness <- read.table("11_MAGs/08_SUMMARY/MAGs-genome-completeness.txt", sep="\t", header = TRUE)

# Create dataframe with genome length
length <- completeness %>%
  select(bin.name, total.length) %>%
  dplyr::rename(MAG = bin.name, length = total.length) %>%
  inner_join(count, by="MAG") %>%
  column_to_rownames(var = "MAG")

# calculate RPK, calculate reads per Kbp of MAG length (corrected for MAG length) MAG length is in bp and converted to Kbp
rpk <- (length[,2:ncol(length)]*10^3/length[,1])
# comparing groups
group <- c(rep("A",ncol(rpk)))
y <- DGEList(counts=rpk, group=group)
# normalize for library size by cacluating scaling factor using TMM (default method)
y <- calcNormFactors(y)
y$samples
# count per million read (normalized count)
norm_counts <- round(as.data.frame(cpm(y)))

norm_counts <- norm_counts %>% rownames_to_column(var = "rowname") %>%
   dplyr::rename(MAG = rowname) 
head(norm_counts)
```

We used DESeq2 for differential MAG abundance analysis between organ.

```
# Create metadata table
metadata <- data.frame(Sample = c("F_Rainbow", "F_SnakePit", "F_TAG", "M_Rainbow", "M_SnakePit", "M_TAG"),
                 Organ = c("F", "F", "F", "M", "M", "M")

# Construct DESeq datadet object
dds <- DESeqDataSetFromMatrix(countData=norm_counts, 
                              colData=metadata, 
                              design=~Organ, tidy = TRUE)

# Run DESeq function
dds <- DESeq(dds)

# Create taxonomy table with gtdb_tk classification
Tax <- read.csv("11_MAGs/09_PHYLOGENO_GTDB/classify_wf_output_gtdbtk_v1.5.0/gtdbtk.bac120.summary.tsv", sep="\t", dec = ".")
Classification <- Tax %>%
  select(user_genome, classification) %>%
  separate(classification, c("Domain", "Phylum", "Class", "Order", "Family", "Genus", "Species"), sep = ";") %>%
  dplyr::rename(MAG = user_genome) %>%
  column_to_rownames(var="MAG")

# Export DESeq result table
res = results(dds, cooksCutoff = FALSE)

```

Differentially abundant MAGs with adjusted p-value of 0.01 and absolute log2 fold change of 1.5 were regarded as significant in this study. We identified 6 MAGs that were significantly differentially abundant between organ and we used a scatterplot to represent them.

```
# Select significant differences
sigtab = res[which(abs(res$log2FoldChange) > 1.5 & res$padj < 0.01), ]
sigtab = cbind(as(sigtab, "data.frame"), as(Classification[rownames(sigtab), ], "matrix"))
sigtab$MAG <-  rownames(sigtab)

# Create table for the plot
sigtab_c <- sigtab %>% 
  mutate(Family = str_replace(Family, "f__", "")) %>%
  mutate(Class = str_replace(Class, "c__", "")) %>%
  mutate(MAG_Family = paste(Family, MAG, sep=" "))

# Order MAGs by log2FoldChange
x = tapply(sigtab_c$log2FoldChange, sigtab_c$MAG_Family, function(x) max(x))
x = sort(x, TRUE)
sigtab_c$MAG_Family = factor(as.character(sigtab_c$MAG_Family), levels=names(x))

# Plot MAGs
c <- ggplot(sigtab_c, aes(log2FoldChange, MAG_Family, size=baseMean, color=Class)) + 
     geom_point() + 
     theme_bw() + 
     xlab("log2 Fold Change") + 
     ylab("MAG") + 
     labs(colour="Class", size="Mean counts") + 
     scale_color_manual(values = c("Bacilli" = "#c9557e", "Clostridia" = "#cc6540", "Deferribacteres" = "#a7943f"))
c

```

![DESeq2](files/Figure2.png)

### Identification of CRISPR arrays and Cas proteins

We used CRISPRCasFinder to identify the CRISPR arrays and Cas proteins in the MAGs

```
mkdir -p /home/datawork-lmee-intranet-nos/REX_Velo/11_MAGs/15_CRISPR

cd /home/datawork-lmee-intranet-nos/REX_Velo/11_MAGs/08_SUMMARY/bin_by_bin/
for file in */*-contigs.fa
do
MAG=$(basename $file | sed 's/-contigs.fa//g')
singularity exec -B /home/datawork-lmee-intranet-nos/singularity/crisprcasfinder,/home/datawork-lmee-intranet-nos/REX_Velo/11_MAGs:/11_MAGs \
            /home/datawork-lmee-intranet-nos/singularity/crisprcasfinder/CrisprCasFinder.simg \
            perl /usr/local/CRISPRCasFinder/CRISPRCasFinder.pl \
            -so /usr/local/CRISPRCasFinder/sel392v2.so -cf /usr/local/CRISPRCasFinder/CasFinder-2.0.3 \
            -drpt /usr/local/CRISPRCasFinder/supplementary_files/repeatDirection.tsv \
            -rpts /usr/local/CRISPRCasFinder/supplementary_files/Repeat_List.csv \
            -cas -def G -out /11_MAGs/15_CRISPR/$MAG -in /11_MAGs/08_SUMMARY/bin_by_bin/$file
done 
```

We then produced a table contaning only contigs with CRISPR or Cas

```
head -n 1 11_MAGs/15_CRISPR/F_Rainbow_MAG_00001/TSV/CRISPR-Cas_summary.tsv > 11_MAGs/15_CRISPR/CRISPR_CAS_EL4.txt

for f in 11_MAGs/15_CRISPR/*/TSV/CRISPR-Cas_summary.tsv; do
    while read line; do
        CRISPR=`echo "$line" | awk -F"\t" '{print $3}'`
        EL=`echo "$line" | awk -F"\t" '{print $4}'`
        if [[ "$CRISPR" == Nb* ]]
            then continue
        elif [[ "$EL" != *Nb_arrays_evidence-level_4=0 ]]
            then echo "$line" >> 11_MAGs/15_CRISPR/CRISPR_CAS_EL4.txt
        fi
    done < "$f"
done
```

The `CRISPR_CAS_EL4.txt` file is available this way:

[CRISPR_CAS_EL4.txt](files/CRISPR_CAS_EL4.txt)

### Taxonomic composition of samples based on the small-subunit rRNA

To explore the taxonomic composition of the community we used phyloFlash (v3.4) together with SILVA database (release 138.1) on the quality-filtered short metagenomic reads.

```
mkdir 12_phyloflash
cd 12_phyloflash

for r1 in ../01_QC/*_R1.fastq.gz
do
# copy the filename, r1, to a new file name, r2 and substitute R1 in the name with R2
# this generates the name of the R2 file
    r2=$r1
    r2="${r1/_R1/_R2}"
# generate the names for the output files
    NAME=$(basename $r1 | sed 's/-QUALITY_PASSED_R1.fastq.gz//g')
# run the phyloFlash command
    phyloFlash.pl -lib $NAME -almosteverything -log -read1 $r1 -read2 $r2 -dbhome /home/datawork-lmee-intranet-nos/database/phyloflash/138.1 -CPUs 56 >> ../00_LOGS/phyloFlash.log 2>&1
done
```

We then compare the results with the `phyloFlash_compare.pl` command:

```
phyloFlash_compare.pl --allzip --task heatmap --log --out phyloflash_REX --outfmt png >> ../00_LOGS/phyloFlash_compare.log 2>&1
```

![phyloFlash](files/FigureS1.png)
